﻿using System;
using System.IO;
using System.Threading;
using Subscribe;
using Xunit;

namespace TestSubscribe
{

    public class UnitTestFolderMonitor: IDisposable
    {

        private Helper helper;

        public void Dispose() => Dispose(true);

        [Fact]
        public void Should_be_shoot_event_on_created_file()
        {
            helper = new Helper(ActionType.forCreate);
           
            FolderMonitor fm = helper.MonitorMock;
            fm.RunMonitor();

            File.Create(helper.TempFile);
            Thread.Sleep(1000);

            Assert.True(helper.EventRaised);
            Assert.Equal(fm, helper.EventSource);
            Assert.True(helper.EventArgs.ChangeTypes == WatcherChangeTypes.Created);
        }

        [Fact]
        public void Should_be_shoot_event_on_delete_file()
        {
            helper = new Helper(ActionType.forDelete);
            
            FolderMonitor fm = helper.MonitorMock;
            fm.RunMonitor();

            helper.DeleteFile();
            Thread.Sleep(1000);

            Assert.True(helper.EventRaised);
            Assert.Equal(fm, helper.EventSource);
            Assert.True(helper.EventArgs.ChangeTypes == WatcherChangeTypes.Deleted);
        }

        [Fact]
        public void Should_be_shoot_event_on_modify_file()
        {
            helper = new Helper(ActionType.forModify);
          
            FolderMonitor fm = helper.MonitorMock;
            fm.RunMonitor();

            helper.ModifyFile();
            Thread.Sleep(1000);

            Assert.True(helper.EventRaised);
            Assert.Equal(fm, helper.EventSource);
            Assert.True(helper.EventArgs.ChangeTypes == WatcherChangeTypes.Changed);
        }

        [Fact]
        public void Should_be_shoot_event_on_rename_file()
        {
            helper = new Helper(ActionType.forModify);

            FolderMonitor fm = helper.MonitorMock;
            fm.RunMonitor();

            helper.RenameFile();
            Thread.Sleep(1000);

            Assert.True(helper.EventRaised);
            Assert.Equal(fm, helper.EventSource);
            Assert.True(helper.EventArgs.ChangeTypes == WatcherChangeTypes.Renamed);
        }

        #region IDisposable 

        private bool disposedValue = false; 

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    helper.Dispose();
                }
                
                disposedValue = true;
            }
        }
        
        #endregion


    }
}
