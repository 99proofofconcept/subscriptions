﻿using System.IO;

namespace Subscriptions
{
    public class ManagingFiles
    {
        protected WatcherChangeTypes change;
        
        protected string KindOfChange
        {
            get { return GetChangeTye(); }
        }

        private string GetChangeTye()
        {
            string message = default(string);

            switch (change)
            {
                case WatcherChangeTypes.Created:
                    message = "File created";
                    break;
                case WatcherChangeTypes.Changed:
                    message = "File modified";
                    break;
                case WatcherChangeTypes.Renamed:
                    message = "File renamed";
                    break;
                case WatcherChangeTypes.Deleted:
                    message = "File removed";
                    break;
            }

            return message;
        }
    }
}
