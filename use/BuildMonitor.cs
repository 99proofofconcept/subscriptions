﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Subscribe;
using Subscribe.Contracts;
using Subscriptions.Subscribers;

namespace Subscriptions
{

    /// <summary>  It supposes there are 3 folders in drive C root named like config file /// </summary>
    internal static class BuildMonitor
    {
        internal static Monitoring FilesTxtInFolder()
        {
            List<FoldersConfiguration> folders = ConfigurationManager.GetSection("pathFiles") as List<FoldersConfiguration>;
            List<string> lstfolders = folders.Select(x => x.Location).ToList();


            IEnumerable<ISubscriber<FolderMonitor>> lstSubs = new List<ISubscriber<FolderMonitor>>()
                       {
                            new ManagingTxtFiles(new BackupFile())
                       };

            List<Folder.Catalogue> LstCatalogues = new List<Folder.Catalogue>();

            lstfolders
               .ForEach((string item) =>
               {
                   LstCatalogues.Add(new Folder.Catalogue(item, lstSubs));
               });

            
            return new Monitoring(LstCatalogues); ;

        }


        internal static Monitoring FilesImageThroughDelegate()
        {
            List<FoldersConfiguration> folders = ConfigurationManager.GetSection("pathImages") as List<FoldersConfiguration>;
            List<string> lstfolders = folders.Select(x => x.Location).ToList();

            ManagingImageFiles.ListenerAction listener = ActionsByKindOfFile.CheckImageFilesInFolder();


            IEnumerable<ISubscriber<FolderMonitor>> lstSubs = new List<ISubscriber<FolderMonitor>>() { new ManagingImageFiles(listener) };

            List<Folder.Catalogue> LstCatalogues = new List<Folder.Catalogue>();

            lstfolders
               .ForEach((string item) =>
               {
                   LstCatalogues.Add(new Folder.Catalogue(item, lstSubs));
               });
            
            return new Monitoring(LstCatalogues);
        }

       
    }
}
