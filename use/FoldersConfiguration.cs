﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;

namespace Subscriptions
{
    public class FoldersConfiguration
    {
        public string Location { get; set; }
    }

    public class DirectoriesSection : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            List<FoldersConfiguration> myConfigObject = new List<FoldersConfiguration>();

            foreach(XmlNode childNode in section.ChildNodes)
            {
                if (childNode.Attributes != null)
                {
                    foreach (XmlAttribute attrib in childNode.Attributes)
                    {
                        myConfigObject.Add(new FoldersConfiguration() { Location = attrib.Value });
                    }
                }
            }

            return myConfigObject;
        }
    }
}
