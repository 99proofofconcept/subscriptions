﻿using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using Subscribe;
using Subscribe.Contracts;

namespace Subscriptions.Subscribers
{
    public class ManagingTxtFiles: ISubscriber<FolderMonitor>
    {
        private readonly IActionsByFile operations;
        
        public ManagingTxtFiles(IActionsByFile operations)
        {
            this.operations = operations;
        }

        public void Subscribe(FolderMonitor source)
        {
            source.FolderChanged += new FolderMonitor.FolderChangeHandler(operations.Run);
        }
        
    }
}
