﻿using System;
using Subscribe;
using Subscribe.Contracts;


namespace Subscriptions.Subscribers
{
    public class ManagingImageFiles : ISubscriber<FolderMonitor>
    {

        public delegate void ListenerAction(object source, FileEventArgs args);

        private readonly ListenerAction listenerSource;

        public ManagingImageFiles(ListenerAction listenerSource)
        {
            if (listenerSource == null)
                throw new ArgumentNullException(nameof(listenerSource));

            this.listenerSource = listenerSource;
        }


        public void Subscribe(FolderMonitor source)
            => source.FolderChanged += new FolderMonitor.FolderChangeHandler(listenerSource);

    }
}
