﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Subscribe.Contracts;

namespace Subscribe
{
    public class Monitoring: IMonitoring, IDisposable
    {
       
        public List<Folder> LstFolderscatalogue { get; private set; } = new List<Folder>();
       
        private List<Folder.Catalogue> lstCatalogues;
        private List<Folder.Catalogue> lstHasbeenAssigned;

        public Monitoring(List<Folder.Catalogue> lstCatalogues)
        {
            this.lstCatalogues = lstCatalogues;
            lstHasbeenAssigned = new List<Folder.Catalogue>();
        }


        public void Start()
        {
            lstCatalogues
               .ForEach((Folder.Catalogue fc) =>
               {
                   if (CanBeMonitored(fc.PathFolder) && !ItWasAssigned(fc.PathFolder))
                   {
                       FolderMonitor folderMonitor = new FolderMonitor(fc.PathFolder);
                       folderMonitor.EnableRaisingEvents = true;

                       fc.Subscribers
                       .ToList()
                        .ForEach((ISubscriber<FolderMonitor> value) =>
                        {
                            value.Subscribe(folderMonitor);
                        });

                       folderMonitor.RunMonitor();

                       Folder folCat = new Folder(folderMonitor, fc);
                       LstFolderscatalogue.Add(folCat);

                       lstHasbeenAssigned.Add(fc);
                   }

               });
        }


        public void Stop() => Dispose();
        

        public void Add(Folder.Catalogue catalogue)
        {
            if (Contains(catalogue.PathFolder))
                throw new ArgumentException(nameof(catalogue));
            
            lstCatalogues.Add(catalogue);
            Start();
        }


        public void Remove(string path)
        {
            if (Contains(path))
            {
                Folder.Catalogue catalogue = lstCatalogues.Find(c => c.PathFolder == path);

                Folder foldCat = LstFolderscatalogue.Find(fc => fc.RecordCatalogue == catalogue);

                foldCat.InstanceMonitor.Dispose();

                LstFolderscatalogue.Remove(foldCat);
                lstCatalogues.Remove(catalogue);
                lstHasbeenAssigned.Remove(catalogue);
            }
        }


        protected bool ItWasAssigned(string path) =>
           lstHasbeenAssigned.Find(fp => fp.PathFolder == path) != null;


        protected bool Contains(string pathFolder) =>
            lstCatalogues.Find(fp => fp.PathFolder == pathFolder) != null;


        protected bool CanBeMonitored(string folder)
        {
            if (!Directory.Exists(folder))
                throw new DirectoryNotFoundException(nameof(folder));

            return true;
        }


        #region IDisposable 

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && LstFolderscatalogue.Count > 0)
                {

                    LstFolderscatalogue
                       .ForEach((Folder fc) =>
                       {
                           fc.InstanceMonitor.Dispose();
                       });

                    LstFolderscatalogue.Clear();
                    LstFolderscatalogue = null;


                    lstCatalogues.Clear();
                    lstCatalogues = null;

                    lstHasbeenAssigned.Clear();
                    lstHasbeenAssigned = null;
                }


                disposedValue = true;
            }
        }

        /// <summary> </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
