﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace Subscribe
{
    public class FileEventArgs : EventArgs
    {
        public string Name { get; set; }
        public string OldName { get; set; }
        public string Path { get; set; }
        public string Extension { get; set; }
        public DateTime AccessTime { get; private set; }
        public WatcherChangeTypes ChangeTypes { get; set; }
        public string LiteralAccessTime { get; private set; }
        public string ReplacementPattern { private get; set; } = "$1.fff";

        public FileEventArgs()
        {
            AccessTime = DateTime.Now;

            string pattern = DateTimeFormatInfo.CurrentInfo.FullDateTimePattern;

            pattern = Regex.Replace(pattern, "(:ss|:s)", ReplacementPattern);

            LiteralAccessTime = DateTime.Now.ToString(pattern);
        }

    }
}
